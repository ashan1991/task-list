<?php include 'header.php'; ?>

    <div class="container">
        <div class="row" id="title">
            <h1 align="center">Task List</h1>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h3 class="usp">People : </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 align="left">Tasks</h3>
            </div>

        </div>
        <div class="row" ng-controller="myTask">
            <div class="container">
                <div class="task" ng-repeat="t in tasks">
                    <div class="row">
                        <div class="col-md-9">
                            <h2>Backend Design</h2>
                            <h5>Ashan Lakshith</h5>
                        </div>
                        <div class="col-md-3">
                            <button align="right" type="button" class="btn btn-primary">Edit</button>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div class="row" ng-controller="adUserDialog">
            <div class="col-md-2 col-md-offset-10">
                <button type="button" class="btn btn-primary btnNewTask">
                    <span class="glyphicon glyphicon-plus-sign"></span> Add New Task
                </button>
            </div>

        </div>


    </div>

<?php include 'footer.php'; ?>